// Command template is a trivial web server that uses the text/template (and
// html/template) package's "block" feature to implement a kind of template
// inheritance.
//
// It should be executed from the directory in which the source resides,
// as it will look for its template files in the current directory.
package main

import (
	"fmt"
	"github.com/mirkar/mygolang/hello/foobar"
	"gitlab.com/golanglab/helloworld/math"
)

func main() {
	fmt.Printf(foobar.Reverse("\n!oG ,olleH"))
	fmt.Printf(foobar.Reverse("\nhello, world"))
	fmt.Printf("hello, world\n")

	xs := []float64{1, 2, 3, 4}
	avg := math.Average(xs)
	fmt.Println(avg)

}
